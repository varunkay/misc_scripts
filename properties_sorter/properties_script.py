import sys
import xlsxwriter
import collections

# Check there is at least one file
if len(sys.argv) >= 1:
    pass
else:
    print("This script needs at least one arguments, aborting")
    exit()

# Insert directory path here
BOX_1 = sys.argv[1] if len(sys.argv) > 1 else ''
BOX_2 = sys.argv[2] if len(sys.argv) > 2 else ''
BOX_3 = sys.argv[3] if len(sys.argv) > 3 else ''
BOX_4 = sys.argv[4] if len(sys.argv) > 4 else ''
BOX_5 = sys.argv[5] if len(sys.argv) > 5 else ''

# Headers for column names
BOXES = [BOX_1, BOX_2, BOX_3, BOX_4, BOX_5]

# Headers for column names
EXCLUDED_PROPERTIES = open('ExcludedProperties.txt', 'r')

# Excel file name
WORKBOOK = xlsxwriter.Workbook('atsPopertiesUpdated.xlsx', {'strings_to_urls': False})


def header_list():
    """
    :return: List of header names from the urls entered
    """
    box_list = ["Property"]
    try:
        for b in BOXES:
            if b.split("/")[-3]:
                box_list.append(b.split("/")[-3])
    except IndexError:
        pass
    return box_list


def load_properties(file_path, sep='=', comment_char='#'):
    """
    Reads the file passed as parameter as a properties file.
    file_path: Insert the full filepath here
    sep: to separate the '=' in string
    comment_char: to remove the '#' commented properties
    """
    props = {}
    with open(file_path, "rt") as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith(comment_char):
                key_value = l.split(sep)
                dict_key = key_value[0].strip()
                dict_value = sep.join(key_value[1:]).strip().strip('"')
                props[dict_key] = dict_value
    return props


def excl_props():
    exc_prop = []
    for e in EXCLUDED_PROPERTIES:
        exc_prop.append(e.rstrip())

    return exc_prop

excluded_properties = excl_props()


def result(load_url):
    """
    :param load_url: Loads the url from the declared urls, eg: QACORE, UAT
    :return: Dictionary containing all the contents of the ats.properties and excluding properties
             from the ExcludedProperties.txt file
    """

    dict_list = load_properties(load_url)
    for e_props in excluded_properties:
        for m in list(dict_list.keys()):
            if e_props.rstrip() == m:
                del dict_list[m]
    return dict_list


def dict_sorter():
    """
    :return: list of Boxes containing initial dicts and Sorted and Unsorted dicts
    """
    sorted_dict = {}
    unsorted_dict = {}
    main_dict_list = []

    # Add additional dicts based on number of boxes/URL
    for bo in BOXES:
        if bo:
            dict_result = result(bo)
            main_dict_list.append(dict_result)

    super_dict = collections.defaultdict(list)

    for d in main_dict_list:
        for k, v in d.items():
            super_dict[k].append(v)

    super_dict = dict(super_dict)

    # FOR COMPARING [val1, val2, val3]
    for k, v in super_dict.items():
        if len(set(v)) == 1 and '' not in set(v) and len(v) == len(main_dict_list):
            sorted_dict[k] = v
            sorted_dict.update(sorted_dict)
        else:
            unsorted_dict[k] = v
            unsorted_dict.update(unsorted_dict)

    list_of_worksheet = [sorted_dict, unsorted_dict]
    zipped_items = [main_dict_list, list_of_worksheet]

    return zipped_items


def xml_writer():
    """
    :return: Writes the xml worksheets
    """
    box_list = header_list()

    dict_sorter_items = dict_sorter()
    list_of_dicts = dict_sorter_items[0]
    list_of_worksheets = dict_sorter_items[1]

    worksheet = WORKBOOK.add_worksheet(name='Sorted Properties')

    sorted_dict = list_of_worksheets[0]
    unsorted_dict = list_of_worksheets[1]

    bold = WORKBOOK.add_format({'bold': 1})
    # Time complexity| unwanted multiple iterations !!!!
    for i, header in enumerate(box_list):
        worksheet.write(0, i, header, bold)

    row = 1
    for key in sorted_dict.keys():
        worksheet.write(row, 0, key)
        # 1: 2nd column(QACore) 3rd(UAT), 4th(STAGING)
        for j, lod in enumerate(list_of_dicts, start=1):
            try:
                worksheet.write_string(row, j, lod[key], cell_format=None)
            except KeyError:
                pass
        row += 1

    worksheet = WORKBOOK.add_worksheet(name='Unsorted Properties')
    row1 = 1
    for key1 in unsorted_dict.keys():
        bold = WORKBOOK.add_format({'bold': 1})
        # Time complexity| unwanted multiple iterations !!!!
        for i, header in enumerate(box_list):
            worksheet.write(0, i, header, bold)

        worksheet.write(row1, 0, key1)
        # 1: 2nd column(QACore) 3rd(UAT), 4th(STAGING)
        for j, lod1 in enumerate(list_of_dicts, start=1):
            try:
                worksheet.write_string(row1, j, lod1[key1], cell_format=None)
            except KeyError:
                pass
        row1 += 1

    WORKBOOK.close()


def main():
    """
    :return: Executes the code
    """
    xml_writer()


if __name__ == "__main__": main()
